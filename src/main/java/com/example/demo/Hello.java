package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * @author Oleg Ushakov. 2019
 */
@Data
public class Hello {
    private final String name;

}
