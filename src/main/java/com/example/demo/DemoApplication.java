package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import reactor.netty.http.server.HttpServer;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(DemoApplication.class, args);
        HttpHandler httpHandler = RouterFunctions.toHttpHandler(getRouter());
        HttpServer
                .create()
                .host("localhost")
                .port(8081)
                .handle((req, resp) -> new ReactorHttpHandlerAdapter(httpHandler).apply(req, resp))
                .bindNow();

        Thread.currentThread().join();

    }

    @SuppressWarnings("unchecked")
    static RouterFunction getRouter() {
        HandlerFunction hello = request -> ok().body(fromObject("Hello"));

        return route(GET("/12"), hello)
                .andRoute(GET("/json"), req -> ok().contentType(APPLICATION_JSON).body(fromObject(new Hello("world"))));
    }

}
